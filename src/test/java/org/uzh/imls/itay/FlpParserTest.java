package org.uzh.imls.itay;

import org.junit.Test;

import java.io.IOException;
import java.io.StringReader;
import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * @author Milan Simonovic <milan.simonovic@imls.uzh.ch>
 */
public class FlpParserTest {

    Parser p = new Parser(new StringReader(flp), new StringReader(l3), new StringReader(affy));


    @Test
    public void testVit4() {
        FLP f = p.getFlpByRefseqId("NM_076211");
        assertEquals(4, f.plus.intValue());
        assertEquals(3, f.minus.intValue());
        assertEquals("vit-4 /// WBGene00006928", f.geneSymbol);
        assertEquals(1, f.ensemblIds.size());
        assertTrue(f.ensemblIds.contains("F59D8.2"));
    }

    @Test
    public void testY48E1B14() {
        FLP nm_064443 = p.getFlpByRefseqId("NM_064443");
        assertEquals(3, nm_064443.plus.intValue());
        assertEquals(2, nm_064443.minus.intValue());
        assertEquals("Y48E1B.14", nm_064443.geneSymbol);

        FLP nm_182150 = p.getFlpByRefseqId("NM_182150");
        assertEquals(1, nm_182150.plus.intValue());
        assertEquals(2, nm_182150.minus.intValue());
        assertEquals("Y48E1B.14", nm_182150.geneSymbol);

        assertEquals(1, nm_064443.ensemblIds.size());
        assertTrue(nm_064443.ensemblIds.contains("Y48E1B.14"));
        assertEquals(1, nm_182150.ensemblIds.size());
        assertTrue(nm_182150.ensemblIds.contains("Y48E1B.14"));

    }

    @Test
    public void testMissingRecords() {
        assertNull(p.getFlpByRefseqId("---"));
    }

    @Test
    public void test_col92_col94() {
        FLP f = p.getFlpByRefseqId("NM_067007");
        assertEquals("col-92 /// col-94 /// WBGene00000667 /// WBGene00000669", f.geneSymbol);
        assertEquals(2, f.ensemblIds.size());
        assertTrue(f.ensemblIds.containsAll(Arrays.asList("W05B2.6", "W05B2.1")));
        assertEquals(2, f.l3s.size());
        assertTrue(f.l3s.contains(new L3("W05B2.1", 19.8382, 465.1209682)));
        assertTrue(f.l3s.contains(new L3("W05B2.6", 22.6835, 531.8310876)));


        f = p.getFlpByRefseqId("NM_067009");
        assertEquals("col-92 /// col-94 /// WBGene00000667 /// WBGene00000669", f.geneSymbol);
        assertEquals(2, f.ensemblIds.size());
        assertTrue(f.ensemblIds.containsAll(Arrays.asList("W05B2.6", "W05B2.1")));
        assertEquals(2, f.l3s.size());
        assertTrue(f.l3s.contains(new L3("W05B2.1", 19.8382, 465.1209682)));
        assertTrue(f.l3s.contains(new L3("W05B2.6", 22.6835, 531.8310876)));
    }

    @Test
    public void test_NM_064943() {
        FLP f = p.getFlpByRefseqId("NM_064943");
        assertNull(f.ensemblIds);
    }


    @Test
    public void test_ensembleVersions() {
        FLP f = p.getFlpByRefseqId("NM_001027088");
        assertEquals(1, f.ensemblIds.size());
        assertTrue(f.ensemblIds.contains("M110.4"));
        assertEquals(2, f.l3s.size());
        assertTrue(f.l3s.contains(new L3("M110.4a", 2.72091, 63.79370576)));
        assertTrue(f.l3s.contains(new L3("M110.4b", 2.91507, 68.34592759)));

    }

    @Test
    public void generateReport() throws IOException {
        p.writeToFile("parser_test.txt");
    }

    final static String affy =
            "vit-4 /// WBGene00006928\tF59D8.2\tNM_076211\n" +

                    "Y48E1B.14\tY48E1B.14\tNM_064443 /// NM_182150\n" +
                    "---\t---\t---\n" +
                    "col-92 /// col-94 /// WBGene00000667 /// WBGene00000669\tW05B2.1 /// W05B2.6\tNM_067007 /// NM_067009\n" +
                    "Y119D3B.13\t---\tNM_064943\n" +
                    "---\tF31C3.10 /// F31C3.9\t---\n" +
                    "WBGene00021898 /// Y54G2A.36\tY54G2A.36 /// Y71A12B.19\tNM_001028330\n" +
                    //5 refseqs to 2 ensembls:
                    "ifg-1\tM110.4\tNM_001027088 /// NM_001027089 /// NM_001136347 /// NM_001136348 /// NM_001136349\n" +
                    "ttn-1\tW06H8.8\tNM_001029027 /// NM_001029028 /// NM_001029029 /// NM_001029030 /// NM_001029031 /// NM_001029032 /// NM_001029033";

    final static String l3 =
//           ifg-1:
            "M110.4a\t2.72091\t63.79370576\n" +
                    "M110.4b\t2.91507\t68.34592759\n" +
//          ttn-1:
                    "W06H8.8a\t0.263829\t6.185662002\n" +
                    "W06H8.8b\t0.271699\t6.370179852\n" +
                    "W06H8.8c\t0.284524\t6.670871267\n" +
                    "W06H8.8d\t0.185379\t4.346344929\n" +
                    "W06H8.8e\t0.261331\t6.127094582\n" +
                    "W06H8.8f\t0.257723\t6.042502409\n" +
                    "W06H8.8g\t0.260961\t6.118419664\n" +
                    //WBGene00021898: Y71A12B.19 doesnt exist
                    "Y54G2A.36\t0.0580717\t1.361533069\n" +

                    //COL-92:
                    "W05B2.1\t19.8382\t465.1209682\n" +
                    "W05B2.6\t22.6835\t531.8310876\n";
    //VIT-4 (F59D8.2) and Y48E1B.14 dont exist


    final static String flp = "NM_001028922\t2\t1\n" +
            "NR_003390\t519\t1588\n" +
            "NM_064943\t0\t2\n" +
            "NM_001028330\t1\t1\n" +
            "NM_067009\t2\t4\n" +
            "NM_067007\t1\t0\n" +
            "NM_076211\t4\t3\n" +
            "NM_182150\t1\t2\n" +
            "NM_064443\t3\t2\n" +

            "NM_001027088\t5\t5\n" +
            "NM_001027089\t7\t7\n" +
            "NM_001136347\t5\t5\n" +
            "NM_001136348\t3\t5\n" +
            "NM_001136349\t3\t4\n" +
            "NM_001029027\t2\t2\n" +
            "NM_001029028\t2\t2\n" +
            "NM_001029029\t14\t13\n" +
            "NM_001029030\t101\t109\n" +
            "NM_001029031\t136\t154\n" +
            "NM_001029032\t136\t154\n" +
            "NM_001029033\t136\t154\n";

}
