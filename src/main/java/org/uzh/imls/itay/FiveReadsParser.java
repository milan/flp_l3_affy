package org.uzh.imls.itay;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.uzh.imls.itay.Parser.*;

/**
 * @author Milan Simonovic <milan.simonovic@imls.uzh.ch>
 */


public class FiveReadsParser {
    public static void main(String[] args) throws Exception {
//        convertExcelExportToFlpFormat();
        parse();
    }

    private static void parse() throws IOException {
        BufferedWriter w = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("flp_l3-unique_reads-result.txt"), "US-ASCII"));
        BufferedReader flpr = new BufferedReader(new InputStreamReader(FiveReadsParser.class.getResourceAsStream("unique_reads.txt"), "US-ASCII"));
        BufferedReader l3r = new BufferedReader(new InputStreamReader(FiveReadsParser.class.getResourceAsStream("L3-ascii.txt"), "US-ASCII"));
        BufferedReader affyr = new BufferedReader(new InputStreamReader(FiveReadsParser.class.getResourceAsStream("Affy-ascii.txt"), "US-ASCII"));

        Map<String, L3> l3s = new HashMap<String, L3>();
        String record;
        while ((record = l3r.readLine()) != null) {
            if (record.trim().isEmpty()) {
                continue;
            }

            String[] columns = record.trim().split(tab);
            String id = columns[0].trim();
            if (missingRecord.equals(id)) {
                continue;
            }
            l3s.put(id, new L3(id, Double.valueOf(columns[1].trim()), Double.valueOf(columns[2].trim())));
        }

        Map<String, Set<String>> ensemblByGeneSymbol = new HashMap<String, Set<String>>();
        while ((record = affyr.readLine()) != null) {
            if (record.trim().isEmpty()) {
                continue;
            }
            String[] columns = record.trim().split(tab);

            for (String id : columns[0].split(idsDelimiter)) {
                id = id.trim();
                if (missingRecord.equals(id)) {
                    continue;
                }
                if (!columns[1].trim().isEmpty()) {
                    Set ensemblIds = new HashSet<String>();
                    for (String eid : columns[1].trim().split(idsDelimiter)) {
                        if (eid.trim().equals(missingRecord)) {
                            continue;
                        }
                        ensemblIds.add(eid.trim());
                    }
                    if (!ensemblIds.isEmpty()) {
                        ensemblByGeneSymbol.put(id, ensemblIds);
                    }
                }
            }
        }


        w.append("FLP identifier\tFLPplus\tFLPminus\tL3 gene name(s)\tL3 dcpm\tL3 norm.\n");
        while ((record = flpr.readLine()) != null)

        {
            if (record.trim().isEmpty()) {
                continue;
            }
            String[] columns = record.trim().split(tab);
            String id = columns[0].trim();
            if (missingRecord.equals(id)) {
                continue;
            }
            FLP f = new FLP(id, Double.valueOf(columns[1].trim()), Double.valueOf(columns[2].trim()));
            f.geneSymbol = id;

            L3 l3 = l3s.get(f.geneSymbol);
            if (l3 != null) {
                appendFlp(w, f);
                appendL3(w, l3);
            } else {
                if (ensemblByGeneSymbol.containsKey(f.geneSymbol)) {
                    for (String ensemblId : ensemblByGeneSymbol.get(f.geneSymbol)) {
                        l3 = l3s.get(ensemblId);
                        if (l3 != null) {
                            appendFlp(w, f);
                            appendL3(w, l3);
                        } else {
                            appendFlp(w, f);
                            appendMissingL3(w);
                        }
                    }
                } else {
                    appendFlp(w, f);
                    appendMissingL3(w);
                }
            }
        }

        w.flush();
        w.close();
        flpr.close();
    }

    private static void appendL3(BufferedWriter w, L3 l3) throws IOException {
        w.append(l3.ensemblId).append(tab).append(l3.dcpm.toString()).append(tab).append(l3.norm.toString()).append("\n");
    }

    private static void appendMissingL3(BufferedWriter w) throws IOException {
        w.append("-\t-\t-").append("\n");
    }

    private static void appendFlp(BufferedWriter w, FLP f) throws IOException {
        w.append(f.geneSymbol).append(tab).append(f.plus.toString()).append(tab).append(f.minus.toString()).append(tab);
    }

    private static void convertExcelExportToFlpFormat() throws IOException {
//        BufferedWriter w = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("flp-5_reads.txt"), "US-ASCII"));
//        BufferedReader flpr = new BufferedReader(new InputStreamReader(FiveReadsParser.class.getResourceAsStream("5reads_per_gene.txt"), "US-ASCII"));

        BufferedWriter w = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("unique_reads.txt"), "US-ASCII"));
        BufferedReader flpr = new BufferedReader(new InputStreamReader(FiveReadsParser.class.getResourceAsStream("unique_reads_orig.txt"), "US-ASCII"));


        String record;
        while ((record = flpr.readLine()) != null) {
            if (record.trim().isEmpty()) {
                continue;
            }
            String[] columns = record.trim().split(tab);
            String id = columns[0].split(" ")[0].trim();
            w.append(id).append(tab);
            w.append(columns[1].trim()).append(tab);
            w.append(columns[2].trim()).append("\n");
        }
        w.flush();
        w.close();
        flpr.close();
    }
}
