package org.uzh.imls.itay;

import java.io.*;
import java.util.*;

/**
 * @author Milan Simonovic <milan.simonovic@imls.uzh.ch>
 */

class FLP {
    String refseqId;
    Double plus;
    Double minus;
    String geneSymbol;
    public Collection<String> ensemblIds;
    public Collection<L3> l3s;

    FLP(String refseqId, Double plus, Double minus) {
        this.refseqId = refseqId;
        this.plus = plus;
        this.minus = minus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FLP flp = (FLP) o;

        if (refseqId != null ? !refseqId.equals(flp.refseqId) : flp.refseqId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return refseqId != null ? refseqId.hashCode() : 0;
    }
}

class L3 {
    String ensemblId;
    Double dcpm;
    Double norm;

    L3(String ensemblId, Double dcpm, Double norm) {
        this.ensemblId = ensemblId;
        this.dcpm = dcpm;
        this.norm = norm;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        L3 l3 = (L3) o;

        if (dcpm != null ? !dcpm.equals(l3.dcpm) : l3.dcpm != null) return false;
        if (ensemblId != null ? !ensemblId.equals(l3.ensemblId) : l3.ensemblId != null) return false;
        if (norm != null ? !norm.equals(l3.norm) : l3.norm != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ensemblId != null ? ensemblId.hashCode() : 0;
        result = 31 * result + (dcpm != null ? dcpm.hashCode() : 0);
        result = 31 * result + (norm != null ? norm.hashCode() : 0);
        return result;
    }
}

public class Parser {
    final static String missingRecord = "---";
    final static String tab = "\t";
    final static String idsDelimiter = new String("///");//  "///"

    Map<String, FLP> flps = new LinkedHashMap();
    protected Map<String, Set<FLP>> flpByEnsembl;


    public Parser(Reader flpReader, Reader l3Reader, Reader affyReader) {
        try {
            parse(new BufferedReader(flpReader), new BufferedReader(l3Reader), new BufferedReader(affyReader));
        } catch (IOException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    public void writeToFile(String output) throws IOException {
        BufferedWriter w = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(output), "US-ASCII"));

        w.append("FLP identifier\tFLPplus\tFLPminus\tAffy gene symbol\tAffy Ensembl\tL3 gene name(s)\tL3 dcpm\tL3 norm.\n");
        for (FLP f : flps.values()) {
            w.append(f.refseqId).append(tab).append(f.plus.toString()).append(tab).append(f.minus.toString()).append(tab);

            if (f.geneSymbol != null) {
                w.append(f.geneSymbol);
            } else {
                w.append("-");
            }
            w.append(tab);

            if (f.ensemblIds != null) {
                Iterator<String> iterator = f.ensemblIds.iterator();
                while (iterator.hasNext()) {
                    String id = iterator.next();
                    w.append(id);
                    if (iterator.hasNext()) {
                        w.append(" ").append(idsDelimiter).append(" ");
                    }
                }
            } else {
                w.append("-");
            }
            w.append("\t");
            if (f.l3s != null && !f.l3s.isEmpty()) {
                Double avgDcpm = 0.d, avgNorm = 0d;
                String name = "";
                Iterator<L3> iterator = f.l3s.iterator();
                while (iterator.hasNext()) {
                    L3 l3 = iterator.next();

                    name += l3.ensemblId;

                    if (iterator.hasNext()) {
                        name += " " + idsDelimiter + " ";
                    }
                    avgDcpm += l3.dcpm / f.l3s.size();
                    avgNorm += l3.norm / f.l3s.size();
                }
                w.append(name).append(tab).append(avgDcpm.toString()).append(tab).append(avgNorm.toString());
            } else {
                w.append("-\t-\t-");
            }
            w.append("\n");
        }

        w.flush();
        w.close();
    }

    protected void parse(BufferedReader flpr, BufferedReader l3r, BufferedReader affyr) throws IOException {
        String record = null;
        while ((record = flpr.readLine()) != null) {
            if (record.trim().isEmpty()) {
                continue;
            }
            String[] columns = record.trim().split(tab);
            String id = columns[0].trim();
            if (missingRecord.equals(id)) {
                continue;
            }
            FLP f = new FLP(id, Double.valueOf(columns[1].trim()), Double.valueOf(columns[2].trim()));
            flps.put(f.refseqId, f);
        }

        while ((record = affyr.readLine()) != null) {
            if (record.trim().isEmpty()) {
                continue;
            }
            String[] columns = record.trim().split(tab);

            for (String id : columns[2].split(idsDelimiter)) {
                id = id.trim();
                if (missingRecord.equals(id)) {
                    continue;
                }
                FLP f = flps.get(id);
                if (f != null) {
                    f.geneSymbol = columns[0].trim();
                    String ensembls = columns[1].trim();
                    if (!ensembls.isEmpty()) {
                        f.ensemblIds = new HashSet<String>();
                        for (String eid : ensembls.split(idsDelimiter)) {
                            if (eid.trim().equals(missingRecord)) {
                                continue;
                            }
                            f.ensemblIds.add(eid.trim());
                        }
                        if (f.ensemblIds.isEmpty()) {
                            f.ensemblIds = null;
                        }
                    }
                }
            }
        }
        flpByEnsembl = new HashMap<String, Set<FLP>>();

        for (FLP f : flps.values()) {
            if (f.ensemblIds != null) {
                for (String id : f.ensemblIds) {
                    if (!flpByEnsembl.containsKey(id)) {
                        flpByEnsembl.put(id, new HashSet<FLP>());
                    }
                    flpByEnsembl.get(id).add(f);
                }
            }
        }

        while ((record = l3r.readLine()) != null) {
            if (record.trim().isEmpty()) {
                continue;
            }
            String[] columns = record.split(tab);
            String id = columns[0].trim();
            if (missingRecord.equals(id)) {
                continue;
            }
            String versionedId = id;
            //check if this is a versioned id:
            if (!flpByEnsembl.containsKey(versionedId) && versionedId.contains(".")) {
                char lastChar = versionedId.charAt(id.length() - 1);
                if (lastChar >= 'a' && lastChar <= 'z') {
                    versionedId = versionedId.substring(0, versionedId.length() - 1);
                }
            }
            if (!flpByEnsembl.containsKey(versionedId) && versionedId.contains(".")) {
                versionedId = versionedId.substring(0, versionedId.lastIndexOf("."));
                if (!flpByEnsembl.containsKey(versionedId) && versionedId.contains(".")) {
                    versionedId = versionedId.substring(0, versionedId.lastIndexOf("."));
                }
            }

            if (flpByEnsembl.containsKey(versionedId)) {
                for (FLP f : flpByEnsembl.get(versionedId)) {
                    if (f.l3s == null) {
                        f.l3s = new HashSet<L3>();
                    }
                    f.l3s.add(new L3(id, Double.valueOf(columns[1].trim()), Double.valueOf(columns[2].trim())));
                }
            } else {

            }
        }

    }


    public FLP getFlpByRefseqId(String refseqId) {
        return flps.get(refseqId);
    }

    public Map<String, FLP> getFlps() {
        return flps;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader flpr = new BufferedReader(new InputStreamReader(Parser.class.getResourceAsStream("FLP-ascii.txt"), "US-ASCII"));
        BufferedReader l3r = new BufferedReader(new InputStreamReader(Parser.class.getResourceAsStream("L3-ascii.txt"), "US-ASCII"));
        BufferedReader affyr = new BufferedReader(new InputStreamReader(Parser.class.getResourceAsStream("Affy-ascii.txt"), "US-ASCII"));

        Parser p = new Parser(flpr, l3r, affyr);
        p.writeToFile("/Users/milans/work/itay/result.txt");

    }

}
